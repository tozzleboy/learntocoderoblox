# GuiBasics

**I see its your first time coding.**
*Today we will learn how to make a simple opening and closing gui*

**To begin please start a new project or if you are in one please follow these steps.**

1. Right click on StarterGui. StarterGUI is where your GUIs are stored and are pushed to the players client upon joining.
2. Select ScreenGUI
 You now have a GUI client to work with.
3. Right click on the ScreenGUI you just created and select create a frame.
4. Resize the frame to your likings and position it correctly (ON YOUR SCREEN FOR IT TO BE ON OTHERS SCREEN GET A GUI RESCALING PLUGIN)
5. Add a TextButton just like we did for the frame and put it in the bottom right.
6. Open the properties tab and scroll down to text, change this text to "null"

You now have created a gui! Fun Right?
Now for the magic too happen.
1. Right click the ScreenGUI once again and insert a local script.
2. Insert this code into the script...

```
local state = closed -- This is how we store a temp variable that can store a little bit of info for example you are creating a log in gui it would store what the person enters in a temp variable then lookup the persons login details on a server to check if they are correct then approve them.
script.Parent.Frame.Visible = false -- This will ensure that the gui is closed when you join, you can change the visible variable in properties anyways.

script.Parent.TextButton.MouseButton1Click:connect(function()
if state == closed then
script.Parent.Frame.Visible = true
state = open
script.Parent.TextButton.Text = "Close"
else
script.Parent.Frame.Visible = false -- Script is defining the script itself, Parent means what folder the script is stored in then frame means the frame you created earlier visible is a property of the frame instance which allows it to be open or closed.
state = closed -- Makes sure the script easily can check that the GUI is not visible.
script.Parent.TextButton.Text = "Open"
end)
```





**TIP: Do not make AdminGuis and store them in StarterGUI I suggest that you copy the gui and make a script that only pushes the gui to Administrators So exploiters cant force open the GUI.**


PLAIN SCRIPT WITHOUT MY TEXT TAMPERING (THIS MAY BE EASIER TO UNDERSTAND)
```
local state = closed 
script.Parent.Frame.Visible = false

script.Parent.TextButton.MouseButton1Click:connect(function()

if state == closed then

script.Parent.Frame.Visible = true

state = open

script.Parent.TextButton.Text = "Close"

else



script.Parent.Frame.Visible = false

state = closed 

script.Parent.TextButton.Text = "Open"

end)

--You dont need to add the spaces its just to help you understand better what each part does so you arent as confused.
```